SoundSpecs CraftBukkit Plugin
=============================
Allows you to play multiple sounds at once, optionally with text
and with optional delays in between each sound.

Copyright (c) 2013 Scott Zeid.  Released under the X11 License.  
<http://code.s.zeid.me/soundspecs>

Configuration
-------------

The SoundSpecs plugin uses several configuration files.  To reload all
configuration files, run `/soundspecs reload`.  To reload just one part
of the configuration, run `/soundspecs reload <category>`, where
`<category>` may be either `config`, `players`, or `sounds`.

### config.yml

`config.yml` contains some general options.

* `sounds`  
  Settings related to sounds.  The sounds themselves are not defined here;
  they are defined in the `sounds/` directory (see below).
  
  * `search-path`  
    This is an array of strings defining a search path to use for sounds
    not found in a YAML file and do not start with a namespace (e.g.
    `minecraft:`).  SoundSpecs will try looking for the sound under each
    entry in the search path until one is found; if none is found, then
    the sound name will be sent intact to the client, but with the
    `minecraft:` namespace added to the beginning.
  
* `player-defaults`  
  Default settings for players.  
  
  * `sound`  
    Whether to send sound to the player.  Defaults to `true`.
  * `text`  
    Whether to send any text defined for a sound to the player.  Defaults
    to `true`.
  

### Players

The `players/` directory contains a YAML file for each player who has
customized their settings.  The YAML file is named `<username>.yml`.  For
the possible settings for a player YAML file, see `player-defaults` under
`bits.yml` above.


### Sounds

The `sounds/` directory contains one or more YAML files, each containing a
list of custom sounds and some properties about them:

* `length`  
  The length of the sound in seconds (floating point).
  
* `text` (optional)  
  Text to be sent at the same time the sound starts playing.
  
* `appendNext` (optional)  
  If this is set to `true`, and another sound with text comes after this
  one in a soundspec, then the text for the next sound will be added to
  the end of this sound's text instead of being sent separately.  The
  default value for this option is `false`.
  

The properties for each sound may be broken up into separate files.  This
is useful, for example, if you have a script that automatically makes a
YAML file with the sound lengths and you do not want it to overwrite any
text you have associated with a sound.  (If you set the same property for
a sound in multiple files, the behavior is undefined.)

Commands
--------

* `/soundspecs`  
  This is the base command.  All other SoundSpecs commands are subcommands
  this command, although `/soundspecs play` does have an alias.
  
  **Permission:**  `soundspecs.command`  
  **Defaults to:**  All players

* `/soundspecs reload [config|sounds|players]`  
  Reloads all configuration files, or if a category name is given, reloads
  only the configuration files relevant to that category.
  
  **Permission:**  `soundspecs.reload`  
  **Defaults to:**  Operators only
  
* `/soundspecs play <soundspec> <player> [further /playsound options...]`  
  `/playsoundspec <soundspec> <player> [further /playsound options...]`  
  Plays the given soundspec to the given player using the built-in
  `/playsound` command.  All arguments after the soundspec are passed
  as-is to the `/playsound` command.
  
  **Permissions:**
  
    - `soundspecs.play`  
      **Defaults to:**  All players
    - `soundspecs.play.others`  
      **Defaults to:**  Operators only
  
* `/soundspecs sound [on|off|?|get] [player]`  
  Turns sound on or off for the calling player or another player.  To show
  the current setting, use `?` or `get`, or simply omit the argument
  altogether if another player is not specified.
  
  **Permissions:**
  
    - `soundspecs.sound`  
      **Defaults to:**  All players
    - `soundspecs.sound.others.*`  
      **Defaults to:**  Operators only
    - `soundspecs.sound.others.get`  
      **Defaults to:**  Operators only
    - `soundspecs.sound.others.set`  
      **Defaults to:**  Operators only
  
* `/soundspecs text [on|off|?|get] [player]`  
  Turns text announcements on or off for the calling player or another
  player.  To show the current setting, use `?` or `get`, or simply omit
  the argument altogether if another player is not specified.
  
  **Permissions:**
  
    - `soundspecs.text`  
      **Defaults to:**  All players
    - `soundspecs.text.others.*`  
      **Defaults to:**  Operators only
    - `soundspecs.text.others.get`  
      **Defaults to:**  Operators only
    - `soundspecs.text.others.set`  
      **Defaults to:**  Operators only
  

Compiling
---------

Before compiling the plugin, you must fetch the project's submodules:

    $ git submodule init
    $ git submodule update --recursive

(Currently, there is only one submodule,
[ConfigManager](http://code.s.zeid.me/configmanager).)

After you do that, to manually build the plugin, run `mvn` from the root
of the source tree.  You will need a working Internet connection in order
for Maven to download the appropriate dependencies.  The compiled JAR
file will be written to `target/SoundSpecs-<version>.jar`.

Be sure to run `git submodule update --recursive` each time you compile
in order to ensure you have the latest versions of each submodule.

Soundspecs
----------

A **soundspec** is a special value that defines one or more sounds to
play, optionally with custom delays in-between.  If a YAML file under
the `sounds` configuration directory contains the length of a given sound,
then SoundSpecs will wait that long before playing the next sound or
waiting a custom delay period.  Otherwise, the next sound or delay will be
played immediately after the vanilla `/playsound` command is sent for the
first sound.

If a YAML file under `sounds/` defines a `text` for the sound, then that
text will be sent to the player if the player has chosen to receive
text.  If `appendNext` is defined for a sound, and a subsequent sound in
the soundspec also defines text, then that sound's text will be appended
to the first sound's text instead of being sent separately.

Soundspecs are semicolon-separated lists, where each element of the list
is either the name of a sound acceptable to the vanilla `/playsound`
command or a delay period in seconds (may be an integer or a non-negative
decimal in standard notation, base 10).

If a search path is specified in `config.yml`, then sounds not defined in
any YAML file under `sounds/` will have each element in the search path
prepended to it until the sound is found.  If the sound name starts with
a namespace, such as `minecraft:`, then the search path will not be
used.  If the sound cannot be found and the sound has no namespace, then
the `minecraft:` namespace will be added to the beginning of the sound
name.

Examples:

* `craftbnay.bits.station.andare.uno`  
  This resolves to `minecraft:craftbnay.bits.station.andare.uno`.

* `tone;arriving;andare.uno;end-of-line_exit`  
  In this example, all four sounds are defined in a YAML file and the
  search path is set to `[craftbnay.bits, craftbnay.bits.station]`.  The
  first sound resolves to `craftbnay.bits.tone`, the second sound
  resolves to `craftbnay.bits.arriving`, the third resolves to
  `craftbnay.bits.station.andare.uno`, and the fourth resolves to
  `craftbnay.bits.end-of-line_exit`.
  
  `arriving` has its `text` set to "§7§lNow arriving:§r  " and also has
  `appendNext` set to `true`.  `andare.uno` has its `text` set to
  "§7Andare Station", and its `appendNext` defaults to `false`.  When
  `arriving` plays, the text string "§7§lNow arriving:§r  Andare Station"
  will be sent to the player all on the same line.  The sound
  `end-of-line_exit` also has `text`, which will be sent on its own line.

* `tone;3;minecraft:mob.creeper.say;2;minecraft:mob.creeper.death`  
  In this example, `tone` is defined in a YAML file with a length and
  resolves to `craftbnay.bits.tone`.  After playing it, SoundSpecs will
  wait three seconds and play `minecraft:mob.creeper.say`.  Since that
  second sound is not defined in a YAML file, the plugin will wait two
  more seconds after the `/playsound` command for the second sound is
  *sent*, not two seconds after the sound has *finished playing*.  After
  those two seconds, `minecraft:mob.creeper.death` is played.

* `3;4;5`  
  This is a useless example.  It will simply cause a thread to wait 3
  seconds, then four seconds, and then five seconds.  The client will
  notice nothing, no sound will be played, and no text will be sent.  This
  is not an error.
