minecraft:
  example.sound1:
    length: 1.9853061224489796
    text:   "Hi"
  example.sound2:
    length: 3.541043083900227
    text:   "I'm a "
    appendNext: true
  example.sound3:
    length: 1.0013605442176872
    text:   "sound!  "
    appendNext: true
  example.sound4:
    length: 3.297233560090703
    text:   "XD"
  example.sound5:
    length: 4.051882086167801
some-mod:
  example.sound6:
    length: 1.9853061224489796
  example.sound7:
    length: 3.541043083900227
  example.sound8:
    length: 1.0013605442176872
  example.sound9:
    length: 3.297233560090703
  example.sound10:
    length: 4.051882086167801
