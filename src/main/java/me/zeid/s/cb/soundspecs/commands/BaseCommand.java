/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   SoundSpecs CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   http://code.s.zeid.me/soundspecs
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.soundspecs.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class BaseCommand {
 public static void sendError(CommandSender sender, String message) {
  sender.sendMessage(ChatColor.RED + message + ChatColor.RESET);
 }
 public static void sendInfo(CommandSender sender, String message) {
  sender.sendMessage(ChatColor.RESET + message);
 }
 public static void sendSuccess(CommandSender sender, String message) {
  sender.sendMessage(ChatColor.GREEN + message + ChatColor.RESET);
 }
 public static void sendWarning(CommandSender sender, String message) {
  sender.sendMessage(ChatColor.YELLOW + message + ChatColor.RESET);
 }
 public static boolean stringToBoolean(String s) {
  s = s.trim().toLowerCase();
  return s.equals("on") || s.equals("1") || s.equals("true") || s.equals("yes");
 }
 public static Long tildeStringToLong(String s) {
  if (s.matches("^~([+-]([0-9]+(\\.([0-9]+)?)?))?$")) {
   String n = s.replaceAll("^~", "");
   return Long.parseLong((!n.equals("")) ? n : "0");
  } else
   return null;
 }
}

