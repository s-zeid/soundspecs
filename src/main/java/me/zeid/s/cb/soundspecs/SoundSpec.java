/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   SoundSpecs CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   http://code.s.zeid.me/soundspecs
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.soundspecs;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.defaults.PlaySoundCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class SoundSpec {
 private String specString;
 private List<Object> spec;
 private boolean sendSound;
 private boolean sendText;
 
 public SoundSpec(String specString) {
  this.specString = specString;
  this.spec       = this.parseSpecString(specString);
  this.sendSound  = true;
  this.sendText   = true;
 }
 public SoundSpec(String specString, ConfigurationSection sounds, String[] searchPath) {
  this.specString = specString;
  this.spec       = this.parseSpecString(specString, sounds, Arrays.asList(searchPath));
  this.sendSound  = true;
  this.sendText   = true;
 }
 public SoundSpec(String specString, ConfigurationSection sounds, List<String> searchPath){
  this.specString = specString;
  this.spec       = this.parseSpecString(specString, sounds, searchPath);
  this.sendSound  = true;
  this.sendText   = true;
 }
 private List<Object> parseSpecString(String specString) {
  return this.parseSpecString(specString, null, Arrays.asList(new String[0]));
 }
 private List<Object> parseSpecString(String specString,
                                      ConfigurationSection sounds,
                                      List<String> searchPath) {
  List<String> parts = Arrays.asList(specString.split(";"));
  List<Object> spec  = new ArrayList<Object>(parts.size());
  char separator = (sounds != null) ? sounds.getRoot().options().pathSeparator() : ':';
  int appendTo = -1;
  for (String part : parts) {
   if (part.matches("^[0-9]+(\\.([0-9]+)?)?$"))
    spec.add((Double) Double.parseDouble(part));
   else {
    if (sounds == null || searchPath == null || searchPath.size() == 0)
     spec.add(part);
    else {
     String key       = this.resolveSound(part, sounds, searchPath);
     Sound  sound     = new Sound(key);
     String keyPrefix = toConfigSound(key, sounds) + String.valueOf(separator);
     Double length    = sounds.getDouble(keyPrefix + "length", 0);
     String text      = sounds.getString(keyPrefix + "text", null);
     if (text != null) {
      text = ChatColor.RESET + text + ChatColor.RESET;
      if (appendTo < 0) {
       spec.add(text);
       if (sounds.getBoolean(keyPrefix + "appendNext", false))
        appendTo = spec.size() - 1;
      } else {
       Object prefix = spec.get(appendTo);
       if (prefix instanceof String) {
        spec.set(appendTo, ((String) prefix) + text);
        if (!sounds.getBoolean(keyPrefix + "appendNext", false))
         appendTo = -1;
       } else {
        appendTo = -1;
        spec.add(text);
       }
      }
     }
     spec.add(sound);
     spec.add(length);
    }
   }
  }
  return spec;
 }
 
 public static String resolveSound(String sound,
                                   ConfigurationSection sounds, String[] searchPath) {
  return resolveSound(sound, sounds, Arrays.asList(searchPath));
 }
 public static String resolveSound(String sound,
                                   ConfigurationSection sounds, List<String> searchPath) {
  // If the unresolved sound contains a colon (namespace separator), assume that
  // the caller does not want us to resolve the sound name against the path
  if (sound.contains(":"))
   return sound;
  if (sounds.getConfigurationSection(sound) == null) {
   String test;
   for (String entry : searchPath) {
    // If the search path entry has a colon in it, then that means a resource pack
    // namespace has been specified in the entry.  We replace the colon with the
    // separator defined in the Configuration for the sounds.  Although the
    // separator for a SoundListConfiguration is normally a colon, we do this
    // anyway just in case.  We change it back to a colon when we return a value.
    //
    // If there is no namespace given in the search path entry, we assume
    // "minecraft" as the default namespace.
    // 
    // If more than one colon happens to be in the search path entry, we drop
    // the second colon and everything after it.
    if (!entry.endsWith(":"))
     test = "." + sound;
    else
     test = sound;
    entry = toConfigSound((entry.contains(":")) ? entry : "minecraft:" + entry, sounds);
    test = entry + test;
    if (sounds.getConfigurationSection(test) != null)
     return toGameSound(test, sounds);
   }
  }
  // Do not add the default namespace if the resolved sound already has one
  if (sound.contains(":"))
   return sound;
  return "minecraft:" + sound;
 }
 public static String toConfigSound(String sound, ConfigurationSection section) {
  String separator = String.valueOf(section.getRoot().options().pathSeparator());
  if (sound.contains(":")) {
   String[] parts = sound.split(":", 3);
   sound = parts[0];
   if (parts.length > 1)
    sound += separator + parts[1];
  }
  return sound;
 }
 public static String toGameSound(String sound, ConfigurationSection section) {
  String separator = String.valueOf(section.getRoot().options().pathSeparator());
  return (!separator.equals(".")) ? sound.replace(separator, ":") : sound;
 }
 
 public boolean sendSound() {
  return this.sendSound;
 }
 public boolean sendText() {
  return this.sendText;
 }
 public SoundSpec sendSound(boolean sendSound) {
  this.sendSound = sendSound;
  return this;
 }
 public SoundSpec sendText(boolean sendText) {
  this.sendText = sendText;
  return this;
 }

 public String toString() {
  return specString;
 }
 
 public void playTo(Player player) {
  playTo(player, new String[0]);
 }
 public void playTo(Player player, String[] playSoundArgs) {
  playTo(player, Arrays.asList(playSoundArgs));
 }
 public void playTo(Player player, List<String> playSoundArgs) {
  ConsoleCommandSender consoleSender = player.getServer().getConsoleSender();
  
  List<String> args = new ArrayList<String>();
  args.add("");
  args.add(player.getName());
  args.addAll(playSoundArgs);
  
  Timer timer = new Timer();
  timer.schedule(new PlayerTask(timer, this.spec.iterator(), args, player, consoleSender,
                                sendSound, sendText),
                 0);
 }
 
 class Sound {
  private String name;
  Sound(String name) {
   this.name = name;
  }
  public String toString() {
   return this.name;
  }
 }
 
 class PlayerTask extends TimerTask {
  private Timer                timer;
  private Iterator<Object>     parts;
  private List<String>         args;
  private Player               player;
  private ConsoleCommandSender consoleSender;
  private boolean              sendSound;
  private boolean              sendText;
  PlayerTask(Timer timer, Iterator<Object> parts, List<String> args,
             Player player, ConsoleCommandSender consoleSender,
             boolean sendSound, boolean sendText) {
   this.timer         = timer;
   this.parts         = parts;
   this.args          = new ArrayList<String>(args);
   this.player        = player;
   this.consoleSender = consoleSender;
   this.sendSound     = sendSound;
   this.sendText      = sendText;
  }
  public void run() {
   if (parts.hasNext() && player.isOnline()) {
    Object part = parts.next();
    long delay = 100L;
    if (part instanceof String && sendText) {
     player.sendMessage((String) part);
    }
    if (part instanceof Sound && sendSound) {
     args.set(0, ((Sound) part).toString());
     new PlaySoundCommand().execute(consoleSender,"playsound",args.toArray(new String[0]));
    }
    if (part instanceof Double) {
     delay = Math.max(100L, (long) Math.round(((Double) part) * 1000));
    }
    timer.schedule(new PlayerTask(timer, parts, args, player, consoleSender,
                                  sendSound, sendText), delay);
   }
   else
    timer.cancel();
  }
 }
}
